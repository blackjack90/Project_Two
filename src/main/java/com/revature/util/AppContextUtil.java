package com.revature.util;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.revature.service.PostService;
import com.revature.service.UserService;

/**
 * Utility class for abstracting usage of ApplicationContext. 
 * <b>Note:</b> Not meant to be used with the web application, only for testing.
 * @author Justin Priester
 *
 */
public class AppContextUtil {
	
	/**
	 * Reference to the applicationContext file.
	 */
	private static ApplicationContext appContext = new ClassPathXmlApplicationContext("applicationContext.xml");
	
	/**
	 * Provides a way to easily obtain the postService bean.
	 * @return postService bean
	 */
	public static PostService getPostService() {
		return appContext.getBean("postService", PostService.class);
	}
	
	/**
	 * Provides a way to easily obtain the userService bean.
	 * @return userService bean
	 */
	public static UserService getUserService() {
		return appContext.getBean("userService", UserService.class);
	}
	
}
