package com.revature.util;

import com.revature.ajax.AjaxMessage;

/**
 * Utility class for easily generating AjaxMessages.
 * @author justi_000
 *
 */
public class AjaxMessageUtil {
	
	/**
	 * Generates an AjaxMessage based on the given code.
	 * @param code to scan for generating the corresponding AjaxMessage
	 * @return generated AjaxMessage with the code and corresponding message
	 */
	public static AjaxMessage getAjaxMessage(int code) {
		switch(code) {
		case FinalUtil.SUCCESS_CODE:
			return new AjaxMessage(code, FinalUtil.SUCCESS_MESSAGE);
		case FinalUtil.USERNAME_AVAILABLE_CODE:
			return new AjaxMessage(code, FinalUtil.USERNAME_AVAILABLE_MESSAGE);
		case FinalUtil.USERNAME_TAKEN_CODE:
			return new AjaxMessage(code, FinalUtil.USERNAME_TAKEN_MESSAGE);
		default: return new AjaxMessage(0, "MESSAGE UNDEFINED");
		}
	}
}
