package com.revature.repository;

import java.util.List;

import com.revature.model.User;

/**
 * Provides connectivity to the User data in the database.
 * @author Justin Priester
 *
 */
public interface UserRepository {
	
	/**
	 * Returns a stored user matching the id of the given user.
	 * @param user Expecting: id
	 * @return User with information from the database matching given User's id
	 */
	public User select(User user);
	
	/**
	 * Returns a stored user matching the username of the given user.
	 * @param user Expecting: username
	 * @return User with information from the database matching given User's username
	 */
	public User selectByUsername(User user);

	/**
	 * Inserts the given user's information into the database.
	 * @param newUser Expecting: any fields (but the id will not matter)
	 */
	public void insert (User newUser);

	/**
	 * Updates the given user's information in the database, matching via id.
	 * @param userToUpdate Expecting: id, any other fields
	 */
	public void update(User userToUpdate);
	
	/**
	 * Deletes a stored user matching the username of the given user.
	 * @param user Expecting: username
	 */
	public void deleteByUsername(User userToDelete);
	
	/**
	 * Returns a stored user like the first name of the given user.
	 * @param user Expecting: firstName
	 * @return Users with information from the database like given User's firstName
	 */
	public List<User> selectAllByFirstName(User user);
	
	/**
	 * Returns a stored user like the last name of the given user.
	 * @param user Expecting: lastName
	 * @return Users with information from the database like given User's lastName
	 */
	public List<User> selectAllByLastName(User user);
	
	/**
	 * Returns a stored user like the first and last name of the given user.
	 * @param user Expecting: firstName, lastName
	 * @return Users with information from the database like given User's firstName and lastName
	 */
	public List<User> selectAllByFullName(User user);
	
	/**
	 * Returns a stored user like the first or last name of the given user.
	 * @param user Expecting: firstName, lastName
	 * @return Users with information from the database like given User's firstName or lastName
	 */
	public List<User> selectAllByFirstOrLastName(User user);
	
	/**
	 * Similar to selectAllByFullName(), but swaps the first and last name in the selection.
	 * @param user Expecting: firstName, lastName
	 * @returnUser Users with information from the database like given User's reversed firstName and lastName
	 */
	public List<User> selectAllByFullNameReverse(User user);
	
	/**
	 * Combines selectAllByFullName() and selectAllByFullNameReverse() into one selection.
	 * @param user Expecting: firstName, lastName
	 * @return Users with information from the database like given User's firstName and lastName or the reverse
	 */
	public List<User> selectAllByFullNameBothWays(User user);
}
