package com.revature.repository;

import java.util.ArrayList;
import java.util.List;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.revature.model.Post;
import com.revature.model.User;
import com.revature.util.FinalUtil;

@Repository("postRepository")
@Transactional
public class PostRepositoryHibernate implements PostRepository {

	@Autowired
	SessionFactory sessionFactory;

	public PostRepositoryHibernate(){};

	@SuppressWarnings("unchecked")
	@Override
	public List<Post> selectByUsername(User user) {
		try{
			return (ArrayList<Post>) sessionFactory.getCurrentSession().createCriteria(Post.class)
					.add(Restrictions.like("postCreator",user))
					.addOrder(Order.asc("createdTime"))
					.list();
		}catch(IndexOutOfBoundsException e){
			return new ArrayList<Post>();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Post> selectAll(Post lastPost) {
		return  sessionFactory.getCurrentSession().createCriteria(Post.class)
				.addOrder(Order.asc("createdTime"))
				.setFirstResult(lastPost.getId() +1)
				.setMaxResults(FinalUtil.MAX_POST_LIST_SIZE)
				.list();
	}

	@Override
	public void insert(Post newPost) {
		sessionFactory.getCurrentSession().save(newPost);
	}

	@Override
	public void delete(Post postToDelete) {
		System.out.println("Post to delete id = " + postToDelete.getId());
		try{
			Session session = sessionFactory.getCurrentSession();
			Post deletedPost = (Post) session.createCriteria(Post.class)
					.add(Restrictions.like("id", postToDelete.getId()))
					.list()
					.get(0);
			session.delete(deletedPost);
		} catch(IndexOutOfBoundsException e) {		
			System.out.println("Dao failed to delete");
		}
	}

	@Override
	public void toggleLikeOff(Post post, User user) {
		post.getUserWhoLiked().remove(user);
		user.getLikesPostId().remove(post);
		post.setLikeCount(post.getLikeCount()-1);
		sessionFactory.getCurrentSession().update(post);
		sessionFactory.getCurrentSession().update(user);
	}

	@Override
	public void toggleLikeOn(Post post, User user) {
		post.getUserWhoLiked().add(user);
		user.getLikesPostId().add(post);
		post.setLikeCount(post.getLikeCount()+1);
		sessionFactory.getCurrentSession().update(post);
		sessionFactory.getCurrentSession().update(user);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Post> selectAllInitial() {
		return  sessionFactory.getCurrentSession().createCriteria(Post.class)
				.addOrder(Order.asc("createdTime"))
				.setMaxResults(FinalUtil.MAX_POST_LIST_SIZE)
				.list();
	}
}
