package com.revature.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.revature.ajax.AjaxMessage;
import com.revature.model.User;
import com.revature.model.UserMisc;
import com.revature.service.UserService;
import com.revature.util.AjaxMessageUtil;
import com.revature.util.FinalUtil;

@Controller
public class UserController {
	
	@Autowired
	private UserService userService;
	
	public UserController() {}
	
	@RequestMapping("/getUser.app")
	public @ResponseBody User getUser(@RequestBody User user) {
		return userService.retrieveUser(user);
	}
	
	@RequestMapping("/searchUsers.app")
	public @ResponseBody List<User> searchUsers(@RequestBody UserMisc userMisc) {
		return userService.findUsers(userMisc);
	}
	
	@RequestMapping("/registerUser.app")
	public @ResponseBody User registerUser(@RequestBody User user) {
		return userService.registerUser(user);
	}
	
	@RequestMapping("/login.app")
	public @ResponseBody User login(HttpServletRequest request, @RequestBody User user) {
		User loggedUser = userService.login(user);
		
		if (loggedUser.getUsername() != null && !loggedUser.getUsername().equals("")) {
			request.getSession().setAttribute("loggedUser", loggedUser);
		}
		
		return loggedUser;
	}
	
	@RequestMapping("/updateUserInformation.app")
	public @ResponseBody User securedUpdateUserInformation(HttpServletRequest request, @RequestBody User user) {
		User loggedUser = (User) request.getSession().getAttribute("loggedUser");
		user.setId(loggedUser.getId());
		return userService.updateUserInformation(user);
	}
	
	@RequestMapping("/updateUserPassword.app")
	public @ResponseBody User securedUpdateUserPassword(HttpServletRequest request, @RequestBody User user) {
		User loggedUser = (User) request.getSession().getAttribute("loggedUser");
		user.setId(loggedUser.getId());
		return userService.updateUserPassword(user);
	}
	
	@RequestMapping("/updateUserProfilePicture.app")
	public @ResponseBody User securedUpdateUserProfilePicture(HttpServletRequest request, @RequestBody User user) {
		User loggedUser = (User) request.getSession().getAttribute("loggedUser");
		user.setId(loggedUser.getId());
		return userService.updateUserProfilePicture(user);
	}
	
	@RequestMapping("/checkUniqueUsername.app")
	public @ResponseBody AjaxMessage checkUniqueUsername(@RequestBody User user) {
		if (userService.isUniqueUsername(user)) {
			return AjaxMessageUtil.getAjaxMessage(FinalUtil.USERNAME_AVAILABLE_CODE);
		} else {
			return AjaxMessageUtil.getAjaxMessage(FinalUtil.USERNAME_TAKEN_CODE);
		}
	}
	
	@RequestMapping("/logout.app")
	public void securedLogout(HttpServletRequest request) {
		request.getSession().invalidate();
	}
	
	@RequestMapping("/resetUserPassword.app")
	public @ResponseBody User resetUserPassword(@RequestBody User user) {
		User resettedUser = userService.resetUserPassword(user);
		userService.emailResetPassword(resettedUser);
		return resettedUser;
	}
	
}
