package com.revature.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.revature.model.Post;
import com.revature.model.User;
import com.revature.service.PostService;

@Controller
public class PostController {
	
	public PostController() {}
	
	@Autowired
	private PostService postService;
	
	@RequestMapping("/getPosts.app")
	public @ResponseBody List<Post> getPosts(@RequestBody Post lastPost){
		return postService.retrieveAllPosts(lastPost);
	}
	
	@RequestMapping("/getInitialPosts.app")
	public @ResponseBody List<Post> getInitialPosts(){
		return postService.retrieveAllInitialPosts();
	}
	
	@RequestMapping("/getUserPost.app")
	public @ResponseBody List<Post> getPostUsername(@RequestBody User user){
		return postService.retrievePostsByUsername(user);
	}
	
	@RequestMapping("/insertPost.app")
	public void insertPost(@RequestBody Post post){
		postService.submitPost(post);
	}
	
	@RequestMapping("/toggleLike.app")
	public void toggleLike(@RequestBody Post post, @RequestBody User user){
		postService.toggleLike(post, user);
	}
}
