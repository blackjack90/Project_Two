package com.revature.service;

import java.util.List;

import com.revature.model.User;
import com.revature.model.UserMisc;

/**
 * Provides business logic for everything about Users.
 * @author Justin Priester
 *
 */
public interface UserService {
	
	/**
	 * Checks if the given user has a username not yet taken by another user.
	 * @param user Expecting: username
	 * @return if the user's username is unique
	 */
	public boolean isUniqueUsername(User user);
	
	/**
	 * Retrieves the full information of the given user.
	 * @param user Expecting: username
	 * @return User with full information
	 */
	public User retrieveUser(User user);
	
	/**
	 * Stores a new user.
	 * @param newUser Expecting: username, password (unhashed), email, firstName, lastName
	 * @return User with full information of the registered user, the password will be hashed
	 */
	public User registerUser(User newUser);
	
	/**
	 * Deletes a user.
	 * @param user Expecting: username
	 */
	public void unregisterUser(User user);
	
	/**
	 * Deletes the user if a user with its username already exists, then stores the user.
	 * @param user Expecting: username
	 * @return User with full information of the registered user, the password will be hashed
	 */
	public User resetUser(User user);
	
	/**
	 * Retrieves the full information of the given user if both the username and password match a stored user.
	 * @param user Expecting: username, password (unhashed)
	 * @return User with full information if username and password match, otherwise an empty User
	 */
	public User login(User user);
	
	/**
	 * Updates a stored user's username, email, first name, and last name matching the id of the given user.
	 * @param userToUpdate Expecting: id, username, email, firstName, lastName
	 * @return User with full information of the updated user
	 */
	public User updateUserInformation(User userToUpdate);
	
	/**
	 * Updates a stored user's password matching the id of the given user.
	 * @param userToUpdate Expecting: id, password (unhashed)
	 * @return User with full information of the updated user
	 */
	public User updateUserPassword(User userToUpdate);
	
	/**
	 * Updates a stored user's profile picture (URL) matching the id of the given user.
	 * @param userToUpdate Expecting: id, profilePicture (URL)
	 * @return User with full information of the updated user
	 */
	public User updateUserProfilePicture(User userToUpdate);
	
	/**
	 * Finds all users filtered by a given name or partial name.
	 * @param userMisc Expecting: name
	 * @return List of all the users filtered by the given name
	 */
	public List<User> findUsers(UserMisc userMisc);
	
	/**
	 * Generates and stores a random password for the given user.
	 * @param user Expecting: username
	 * @return User with full information of the updated user, including the new password unhashed
	 */
	public User resetUserPassword(User user);
	
	/**
	 * Sends an email to the given user's stored email address about their newly generated password.
	 * Expected to be used in conjunction with resetUserPassword().
	 * @param user User returned from resetUserPassword()
	 */
	public void emailResetPassword(User user);
	
}
