/** MY BUCKET UPLOAD POLICY
 * {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "Stmt1502394348000",
            "Effect": "Allow",
            "Action": [
                "s3:PutObject"
                "s3:PutObjectAcl"
            ],
            "Resource": [
                "arn:aws:s3:::this-is-dirks-test-bucket789/*"
            ]
        }
    ]
}
 */
var app = angular.module('uploading', ['controllers', 'directives']);

var controllers = angular.module('controllers', []);

controllers.controller('UploadController',['$scope', function($scope) {
	$scope.creds = {
	  bucket: 'this-is-dirks-test-bucket789',
	  access_key: 'AKIAIW52XRW3NFT4OYJQ',
	  secret_key: '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'
	}
	 
	$scope.upload = function() {
	  // Configure The S3 Object 
	  AWS.config.update({ accessKeyId: $scope.creds.access_key, secretAccessKey: $scope.creds.secret_key, signatureVersion: 'v4' });
	  AWS.config.region = 'us-east-2';
	  var bucket = new AWS.S3({ params: { Bucket: $scope.creds.bucket } });
	  console.log("After bucket creation: ", typeof(bucket));
	 
	  if($scope.file) {
	    var params = { Key: $scope.file.name, ContentType: $scope.file.type, Body: $scope.file, ServerSideEncryption: 'AES256' };
	 
	    bucket.putObject(params, function(err, data) {
	      if(err) {
	        // There Was An Error With Your S3 Config
	        console.log(err.message);
	        return false;
	      }
	      else {
	        // Success!
	        console.log('Upload Done');
	      }
	    })
	    .on('httpUploadProgress',function(progress) {
	          // Log Progress Information
	          console.log(Math.round(progress.loaded / progress.total * 100) + '% done');
	        });
	  }
	  else {
	    // No File Selected
	    console.log('No File Selected');
	  }
	}
}]);